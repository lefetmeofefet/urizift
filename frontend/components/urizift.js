import UriziftClient from "../uriziftClient.js";

const {render, html} = lighterhtml;

customElements.define("urizift-app", class extends HTMLElement {
    get canVote() {
        return !this.loading;
    }

    constructor() {
        super();
        this.rating = 0;
        this.votes = 0;
        this.loading = true;

        this.render = render.bind(
            // used as update callback context
            this,
            // used as target node
            // it could either be the node itself
            // or its shadow root, even a closed one
            this.attachShadow({mode: 'open'}),
            // the update open callback
            this._render
        );
        // first render
        this.render();

        // Resize font
        const oneCharFont = 820;
        let fontSize = oneCharFont / UriziftClient.getDisplayWord().length;
        fontSize = Math.min(fontSize, 150);
        this.shadowRoot.querySelector("#title").style.fontSize = `${fontSize}px`;

        (async () => {
            await this.loadRatings();
            this.loading = false;
            this.render();
        })();
    }

    async loadRatings() {
        const {votes, rating, distribution} = await UriziftClient.getRating();
        this.votes = votes;
        this.rating = rating;
        this.distribution = distribution;
    }

    async vote(rating) {
        if (this.canVote && UriziftClient.getUserRating() !== rating) {
            this.hoveredRating = null;
            this.loading = true;
            this.render();

            await UriziftClient.vote(rating);
            await this.loadRatings();
            this.loading = false;
            this.render();
        }
    };

    _render() {
        return html`
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
:host {
    --golden-color: goldenrod;
    
    display: flex;
    height: 100%;
    background-color: #d6d6d6;
    flex-direction: column;
    align-items: center;
    justify-content: center;
}

#container {
    margin-bottom: 10%;
}

#title {
    font-size: 150px;
    font-weight: bold;
    margin-bottom: 20px;
    text-align: center;
    color: #3c3c3c;
}

#stars {
    display: flex;
    justify-content: center;
}

#rating-container {
    font-weight: bold;
    display: flex; 
    margin-top: 17px;
    font-size: 30px;
    color: #aaaaaa;
}

</style>

<div id="container">
    <div id="title">
        ${UriziftClient.getDisplayWord()}
    </div>
    
    <div id="stars">
        ${this.renderStars()}
    </div>
    <div id="rating-container">
        ${this.renderDistribution()}
        ${this.renderStats()}    
    </div>
</div>

`;
    }

    renderStars() {
        return html`
<style>
.star-container{
    width: 80px;
    height: 80px;
    font-size: 80px;
    justify-content: center;
    padding: 0 10px;
    display: flex;
}

.star-container.voteable:hover {
    cursor: pointer;
}

.background-star {
    position: absolute;
    color: #b8b8b8;
    width: inherit;
}
.non-background-star {
    z-index: 1;
    color: var(--golden-color);
    width: inherit;
}

</style>    

${this.renderStarsList()}
        
        `;
    }

    renderStarsList() {
        let starsRoundedRating = null;

        if (this.canVote && this.hoveredRating) {
            starsRoundedRating = this.hoveredRating;
        } else if (this.loading) {
            starsRoundedRating = 0;
        } else {
            starsRoundedRating = this.rating;
            if (starsRoundedRating % 0.5 > 0.5 / 2) {
                starsRoundedRating += 0.5 - starsRoundedRating % 0.5
            } else {
                starsRoundedRating -= starsRoundedRating % 0.5
            }
        }

        return new Array(5).fill(0).map((_, index) => {
            let starRendering = null;
            let currentStar = index + 1;
            if (currentStar <= starsRoundedRating) {
                starRendering = html`<i class="fa fa-star non-background-star"></i>`;
            } else if (currentStar - 0.5 <= starsRoundedRating) {
                starRendering = html`<i class="fa fa-star-half non-background-star"></i>`;
            } else {
                starRendering = html``;
            }
            return html`
   
<div class=${`star-container ${this.canVote ? "voteable" : ""}`}
     onmouseenter=${() => this.onMouseOverStar(index)}
     onmouseleave=${() => this.onMouseOutOfStar()}
     onclick=${() => this.vote(index + 1)}
     >
    <i class="background-star fa fa-star"></i>
    ${starRendering}
</div>
            
            `;
        })
    }

    onMouseOverStar(index) {
        if (this.canVote) {
            this.hoveredRating = index + 1;
            this.render();
        }
    }

    onMouseOutOfStar() {
        this.hoveredRating = null;
        this.render();
    }

    renderDistribution() {
        return html`

<style>
#rating-distribution {
    flex: 1;
    margin-right: 20px;
}

.distribution-rating-number {
    color: #868686;
    font-size: 16px;
    margin-right: 8px;
}

.distribution-row {
    display: flex;
    padding: 4px 0;
}

.distribution-bar {
    display: flex;
    flex: 1;
    background-color: #0000001f;
    height: 12px;
    align-self: center;
    border-radius: 666px;
}

.distribution-bar>.full-bar {
    border-radius: inherit;
    background-color: var(--golden-color);
}
.distribution-bar.user-selected>.full-bar {
    border-radius: inherit;
    background-color: #49adff;
}
</style>
<div id="rating-distribution">
    ${this.renderDistributionBars()}
</div>
        
        `
    }

    renderDistributionBars() {
        let maxRow = !this.loading && Math.max(...this.distribution);
        return [5, 4, 3, 2, 1].map(number => {
            const votesForNumber = !this.loading ? this.distribution[number - 1] : 0;
            return html`
        
<div class="distribution-row">
    <div class="distribution-rating-number">${number}</div>
    <div class=${`distribution-bar ${UriziftClient.getUserRating() === number ? "user-selected" : ""}`} 
         title=${votesForNumber}>
        ${this.renderDistributionBar(!this.loading ? votesForNumber / maxRow : 0)}
    </div>
</div>
            
            `
        });
    }

    renderDistributionBar(percent) {
        return html`

<div class="full-bar" 
     style=${`width: ${Math.round(percent * 100)}%`}>
</div>
        
        `
    }

    renderStats() {
        return html`

<style>
#rating-right-side {
    min-width: 140px;
    display: flex;
    flex-direction: column;
    width: fit-content;
}

#rating-text,#vote-count {
    flex: 1;
    justify-content: center;
    display: flex;
}

#rating-text {
    font-size: 78px;
    color: #909090;
    align-items: center;
    letter-spacing: -7px;
}

#vote-count {
    color: #999999;
    align-items: center;
    font-size: 18px;
}

</style>

<div id="rating-right-side">
    <div id="rating-text">${this.loading ?
            html`<div class="fa fa-spinner fa-spin" style="margin-top: 20px;"></div>`
            :
            html`${this.rating.toFixed(1)}`
            }
    </div>
    <div id="vote-count"> ${this.loading ?
            html``
            :
            html`${(this.votes.toString() + " Votes")}`
            }
    </div>
</div>
        
        `
    }
});