window.onError = error => {
    console.error("Error catcher wasn't bootstrapped, and error was thrown: ", error);
};

window.addEventListener("error", function (e) {
    onError({
        message: e.error.message,
        stack: e.error.stack,
    });
    return false;
}, {useCapture: true});

window.addEventListener('unhandledrejection', function (e) {
    onError({
        message: e.reason.message,
        stack: e.reason.stack,
    });
});
