import swaggerUi from 'swagger-ui-express'
import swaggerJSDoc from "swagger-jsdoc"

const swaggerOptions = {
    definition: {
        openapi: '3.0.0', // Specification (optional, defaults to swagger: '2.0')
        info: {
            title: 'UriziftAPI Dorkumentation', // Title (required)
            version: '1.0.0', // Version (required)
        },
    },
    // Path to the API docs
    apis: [
        '../swagger/swagger.js',
        './server.mjs'
    ],
};
const swaggerSpec = swaggerJSDoc(swaggerOptions);

function swaggerify(app) {
    app.use(['/docs', '/api-docs', '/swagger', '/schweigger'], swaggerUi.serve, swaggerUi.setup(swaggerSpec));
}

export default swaggerify

// Definitions
/**
 * @swagger
 * tags:
 *   - name: words
 *     description: All gyms must die
 *
 * components:
 *   schemas:
 *     SomeSchema:
 *       type: object
 *       required:
 *         - someObject
 *       properties:
 *         someObject:
 *           type: string
 *
 */