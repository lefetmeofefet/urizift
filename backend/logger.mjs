const logger = new class {
    log() {
        console.log(...arguments);
    }
    warn(message) {
        console.warn(...arguments);
    }
    error(message) {
        console.error(...arguments);
    }
    logResponseTime(req, res, next) {
        let start = process.hrtime();
        res.on('finish', () => {
            let timePassed = process.hrtime(start);
            let seconds = (timePassed[0] + (timePassed[1] / 1e9)).toFixed(3);
            logger.log(`Request ${req.url} took ${seconds}s to complete. body: `, req.body);
        });
        next();
    }
};

export default logger