import express from "express";
// This import makes async errors be caught instead of passing silently
import 'express-async-errors';
import swaggerify from "./backend/swagger.mjs"
import logger from "./backend/logger.mjs"
import UriziftDb from "./backend/uriziftDb.mjs";

let app = express();

// Swagger
swaggerify(app);

// Express JSON parsers
app.use(express.json());

// Serve frontend
app.get("/", (request, response, next) => response.redirect('/nothing'));
app.use("/:word", express.static("frontend"));

// Logger
app.use(logger.logResponseTime);

// app.use("/members", membersApi);


app.get('/lifecheck', (request, response) => {
    response.json("im alife");
});

app.post('/get_rating', async (request, response) => {
    let userUniqueIdentifier = request.body.uniqueId;
    let word = request.body.word;
    let rating = await UriziftDb.getRating(word, userUniqueIdentifier);
    response.json(rating);
});

app.post('/vote', async (request, response) => {
    let userUniqueIdentifier = request.body.uniqueId;
    let word = request.body.word;
    let rating = request.body.rating;
    await UriziftDb.vote(userUniqueIdentifier, word, rating);
    response.json({})
});

app.post('/reportError', async (request, response) => {
    let error = request.body.error;
    let userAgent = request.body.userAgent;
    await UriziftDb.reportError(error, userAgent);
    response.json({})
});

// Error catcher
app.use((err, req, res, next) => {
    // Fourth parameter means that this receives errors that happened before
    logger.error("Error occured:", err);
    res.status(500).send(err)
});

//The 404 Route (ALWAYS Keep this as the last route)
app.get('*', function(req, res){
    res.redirect("/nothing");
});

// Create server
let server = app.listen(process.env.PORT || 8008, () => {
    let {port, address} = server.address();
    if (address === "::") {
        address = "http://localhost"
    }
    console.log(`Urizift is UP! ${address}:${port}`)
});
