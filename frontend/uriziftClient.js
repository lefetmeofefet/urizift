

export default new class UriziftClient {
    get word() {
        return this._word
    }

    getDisplayWord() {
        return this._word.substring(0, 1).toUpperCase() + this._word.substring(1).toLowerCase()
    }

    constructor() {
        this._word = UriziftClient.getCurrentWordFromUrl();
        this.uniqueId = localStorage.uniqueId || UriziftClient._generateId();
        localStorage.uniqueId = this.uniqueId;
        this.userRating = 0;

        // Log errors into DB
        window.onError = error => {
            this.constructor.post("/reportError", {
                error: error,
                userAgent: navigator.userAgent
            })
        };
    }

    static getCurrentWordFromUrl() {
        let word = window.location.pathname.substring(1, window.location.pathname.length - 1);
        word = word.toLowerCase();
        return word;
    }

    getUserRating() {
        return this.userRating;
    }

    static _generateId() {
        return Math.random().toString(36).substring(2, 15) +
            Math.random().toString(36).substring(2, 15);
    }

    async getRating() {
        let response = await this.constructor.post("/get_rating", {
            uniqueId: this.uniqueId,
            word: this.word
        });
        console.log("get_rating response: ", response);
        this.userRating = response.userRating;
        return {
            rating: response.rating,
            votes: response.votes,
            distribution: response.distribution
        }
    }

    async vote(rating) {
        await this.constructor.post("/vote", {
            uniqueId: this.uniqueId,
            word: this.word,
            rating
        });
    }

    static async post(url, body) {
        const response = await fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify(body),
        });
        return await response.json()
    }
};
