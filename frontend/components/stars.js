const {render, html} = lighterhtml;

customElements.define("rating-stars", class extends HTMLElement {
    constructor() {
        super();
        this.render = render.bind(
            // used as update callback context
            this,
            // used as target node
            // it could either be the node itself
            // or its shadow root, even a closed one
            this.attachShadow({mode: 'open'}),
            // the update open callback
            this._render
        );
        // first render
        this.render();
    }

    connectedCallback(){
        console.log("Connected stars");
    }

    _render() {
        console.log("Rendering stars. canVote: ", this.canVote, "rating", this.rating);
        return html`

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
:host {
    display: flex;
}

.star-container{
    width: 60px;
    height: 60px;
    font-size: 60px;
    padding: 0 10px;
    display: flex;
}

.star-container.voteable:hover {
    cursor: pointer;
}

.background-star {
    position: absolute;
    color: #b8b8b8;
}
.non-background-star {
    z-index: 1;
    color: goldenrod;
}

</style>    

${this.renderStars()}
        
        `;
    }

    renderStars() {
        let starsRoundedRating = (this.canVote && this.hoveredRating) || this.rating;
        if (starsRoundedRating % 0.5 > 0.5 / 2) {
            starsRoundedRating += 0.5 - starsRoundedRating % 0.5
        } else {
            starsRoundedRating -= starsRoundedRating % 0.5
        }
        return new Array(5).fill(0).map((_, index) => {
            let starRendering = null;
            let currentStar = index + 1;
            if (currentStar <= starsRoundedRating) {
                starRendering = html`<i class="fa fa-star non-background-star"></i>`;
            } else if (currentStar - 0.5 <= starsRoundedRating) {
                starRendering = html`<i class="fa fa-star-half non-background-star"></i>`;
            } else {
                starRendering = html``;
            }
            return html`
   
<div class=${`star-container ${this.canVote ? "voteable" : ""}`}
     onmouseenter=${() => this._mouseOverStar(index)}
     onmouseleave=${() => this._mouseOutOfStar()}
     onclick=${() => this._vote(index + 1)}
     >
    <i class="background-star fa fa-star"></i>
    ${starRendering}
</div>
            
            `;
        })
    }

    _vote = rating => {
        if (this.canVote) {
            console.log("oting");
            this.vote(rating)
        }
    };

    _mouseOverStar(index) {
        if (this.canVote) {
            this.hoveredRating = index + 1;
            this.render();
        }
    }

    _mouseOutOfStar() {
        if (this.canVote){
            this.hoveredRating = null;
            this.render();
        }
    }
});
