import pg from 'pg'
import logger from "./logger.mjs";

const DB_CONNECTION_DETAILS = {
    host: 'manny.db.elephantsql.com',
    database: 'rhrbmoep',
    user: 'rhrbmoep',
    password: 'qo8UygMf3wMXBgrPTqSfe06GfAJS_BDB',
    port: 5432,
    max: 4
};

const db = new class {
    constructor() {
        this.pool = new pg.Pool(DB_CONNECTION_DETAILS);

        this.pool.on('error', (err, client) => {
            console.error('Unexpected error on idle postgres client', err, client);
            process.exit(-1)
        });
    }

    async query(query) {
        return await this.pool.query(query);
    }

    async querySingle(query) {
        return (await this.pool.query(query)).rows[0]
    }

    async transaction(queryCbPromise) {
        const client = await this.pool.connect();

        try {
            await client.query('BEGIN');
            await queryCbPromise(client);
            await client.query('COMMIT')
        } catch (e) {
            await client.query('ROLLBACK');
            throw e
        } finally {
            client.release()
        }
    }
};


export default new class UriziftDb {
    async getRating(word, userUniqueIdentifier) {
        await this._addWordIfNotExists(word);
        await this._addVoterIfNotExists(userUniqueIdentifier);
        await this._addVisit(userUniqueIdentifier, word);
        let rating = await db.querySingle({
            name: 'get-votes',
            text: `
              SELECT avg(votes.rating)                                       as rating,
                     count(votes.id)                                         as votes,
                     count(CASE WHEN votes.rating = 5 then votes.rating end) as voted_5,
                     count(CASE WHEN votes.rating = 4 then votes.rating end) as voted_4,
                     count(CASE WHEN votes.rating = 3 then votes.rating end) as voted_3,
                     count(CASE WHEN votes.rating = 2 then votes.rating end) as voted_2,
                     count(CASE WHEN votes.rating = 1 then votes.rating end) as voted_1,
                     (
                       select votes.rating
                       from voters
                              inner join votes on votes.voter_id = voters.id
                              inner join words on words.id = votes.word_id
                       where voters.unique_identifier = $2
                         and words.word = $1
                     )                                                       as user_rating
              FROM votes
                     inner join words on words.id = votes.word_id
              where words.word = $1
            `,
            values: [word, userUniqueIdentifier]
        });
        if (rating.rating === null) {
            rating.rating = '0';
        }
        return {
            rating: parseFloat(rating.rating),
            userRating: rating.user_rating,
            votes: parseInt(rating.votes),
            distribution: [
                parseInt(rating.voted_1),
                parseInt(rating.voted_2),
                parseInt(rating.voted_3),
                parseInt(rating.voted_4),
                parseInt(rating.voted_5)
            ]
        }
    }

    async _addWordIfNotExists(word) {
        return await db.query({
            name: 'add-word-if-not-exists',
            text: `
              INSERT INTO words (word)
              values ($1)
              ON CONFLICT
                 DO NOTHING;
            `,
            values: [word]
        })
    }

    async _addVoterIfNotExists(userUniqueIdentifier) {
        return await db.query({
            name: 'add-voter-if-not-exists',
            text: `
              INSERT INTO voters (unique_identifier)
              values ($1)
              ON CONFLICT
                 DO NOTHING;
            `,
            values: [userUniqueIdentifier]
        })
    }

    async _addVisit(userUniqueIdentifier, word) {
        return await db.query({
            name: 'add-visit',
            text: `
              INSERT INTO visits (word_id, voter_id, visit_time) 
                (
                  SELECT words.id,
                         voters.id,
                         now()
                  FROM words
                         INNER JOIN voters on 1 = 1
                  where words.word = $2
                    and voters.unique_identifier = $1
                )
            `,
            values: [userUniqueIdentifier, word]
        })
    }

    async vote(userUniqueIdentifier, word, rating) {
        await this._deleteVoteForWordIfExists(userUniqueIdentifier, word);
        return await db.querySingle({
            name: "vote-vote",
            text: `
              INSERT INTO votes (voting_time, rating, voter_id, word_id)
                (
                  SELECT now(),
                         $3,
                         voters.id,
                         words.id
                  FROM words
                         INNER JOIN voters on 1 = 1
                  where words.word = $2
                    and voters.unique_identifier = $1
                )
            `,
            values: [userUniqueIdentifier, word, rating]
        });
    }

    async _deleteVoteForWordIfExists(userUniqueIdentifier, word) {
        return await db.query({
            name: 'delete-vote-for-word-if-not-exists',
            text: `
              DELETE
              from votes
              WHERE votes.voter_id = (SELECT id FROM voters where unique_identifier = $1)
                and votes.word_id = (SELECT id FROM words where word = $2)
            `,
            values: [userUniqueIdentifier, word]
        })
    }

    async reportError(error, userAgent) {
        return await db.query({
            name: 'report-error',
            text: `
              INSERT into errors (error, user_agent, report_time) 
              values ($1, $2, now())
            `,
            values: [error, userAgent]
        })
    }
}
